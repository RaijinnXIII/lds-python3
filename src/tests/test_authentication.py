import pytest

from src import authentication

"""
Unit Tests:
    Module: authentication
"""

@pytest.fixture
def test_Credentials():
    """
    Description:
        Provides a fresh Credentials instance for unit tests
    """
    return authentication.Credentials()

@pytest.fixture
def test_Client():
    """
    Description:
        Provides a fresh Client instance for unit tests
    """
    return authentication.Client()

def test_EdgeGridAuth_inherits(test_Client):
    """
    Description:
        Verify Client() inherits EdgeGridAuth class
    """
    from akamai.edgegrid import EdgeGridAuth
    assert isinstance(test_Client, EdgeGridAuth)
