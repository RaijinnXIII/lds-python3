"""
    Module: delivery_helpers
    Description:
        Utility functions dedicated to streamlining processes surrounding LDS delivery specs
"""

def validate_delivery_type(delivery_type: str) -> None:
    """
    Ensure 'delivery_type' argument is a valid option

    :param delivery_type: String containing either "email" or "ftp" delivery option
    :return None
    :raise ValueError
    """
    allowed = ['email', 'ftp']
    if delivery_type not in allowed:
        raise ValueError(f'Invalid delivery_type: {delivery_type}')
