"""
    Module: source_helpers
    Description:
        Helper functions intended to streamline validation & use of LDS Log Sources
"""

# TODO: Create 'allowed' dynamically by retrieving schema
def validate_log_source_type(log_source_type: str) -> None:
    """
    Ensure 'log_source_type' argument is part of known, valid Log Source Types,
    includes static references to API

    :param log_source_type: Name of log source type defined by LDS API
    :return None
    :raise ValueError
    """
    allowed = [
        'answerx',
        'answerx-objects',
        'cpcode-products',
        'edns',
        'edns-zones',
        'etp',
        'gtm',
        'gtm-properties',
    ]
    if log_source_type not in allowed:
        raise ValueError(f'Invalid Log Source Type: {log_source_type}')
