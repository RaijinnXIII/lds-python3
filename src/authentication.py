from akamai.edgegrid import EdgeGridAuth


class Credentials:
    """
    authentication.Credentials

    Description:
        Detect, validate, and load Akamai OPEN API credentials

    Intended Use:
        Provide valid Credentials instance to Client for authentication
    """
    __slot__ = {
        "AKAMAI_ACCESS_TOKEN": str,
        "AKAMAI_BASE_URL": str,
        "AKAMAI_CLIENT_ID": str,
        "AKAMAI_CLIENT_SECRET": str,
        "AKAMAI_CLIENT_TOKEN": str,
    }

    def __init__(self):
        pass


    def check_for_auth_file(filename: str = '.edgerc') -> None:
        """
        Attempt to retrieve credentials file by filename,
        if available, continue thread, else raise OSError

        :param filename: Name of credentials file within project root directory
        :return None
        :raise OSError
        """
        pass

    def check_for_env_vars() -> None:
        """
        Check presence of expected ENV vars containing credentials,
        if available, continue thread, else raise OSError

        :return None
        :raise OSError
        """
        pass

    @staticmethod
    def load_from_file(filename: str = '.edgerc', check: bool = True):
        """
        Create Credentials instance after retrieving from .edgerc type file
        
        :param filename: Credentials filename, defaults to '.edgerc'
        :param filename: str, optional
        :param check: Ensure file exists before loading, defaults to True
        :param check: bool, optional
        :return: Usable dictionary of creds for authentication.Client
        :rtype: Credentials
        """
        pass

    @staticmethod
    def load_from_env(check: bool = True):
        """
        Create Credentials instance after retrieving env vars
        
        :param check: Ensure expected env vars exist, defaults to True
        :param check: bool, optional
        :return: Usable dictionary of creds for authentication.Client
        :rtype: Credentials
        """
        pass

class Client(EdgeGridAuth):
    """
    authentication.Client
    
    Description:
        Streamline client authentication process for EdgeGridAuth

    :param EdgeGridAuth: Original client being wrapped; dependency
    :type EdgeGridAuth: module

    Intended Use:
        - Expects valid Credentials object (optional)
        - Handles interaction between creds and EdgeGridAuth client
        - Handles URL formation for authorized API access
    """
    __creds__ = None

    def __init__(self, credentials: Credentials = None):
        """
        Description:
            Initialize a new authentication.Client instance
        
        :param credentials: Valid Credentials object, defaults to None
        :param credentials: Credentials, optional
        """
        pass
