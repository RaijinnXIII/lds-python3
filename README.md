Akamai LDS (v3) Wrapper
=======================
Requires Python 3.7+


Description
===========
This project's goal is to streamline interactions between an authorized API client, granted within Akamai's Luna Control Center, and the Log Delivery Service (v3) API.

The LDS API allows the delivery of edge traffic logs to an email or FTP server, and the intention of this wrapper is to make the process of administering changes to this API more simple.


References
============

Getting Started with Akamai APIs: [Docs](https://developer.akamai.com/api/getting-started)

Akamai Log Delivery Service API: [Docs](https://developer.akamai.com/api/core_features/log_delivery_service/v3.html)

Basic Usage
===========

```python
# Import lds_wrapper.authentication module
from lds_wrapper import authentication

# Load credentials from file (default: .edgerc)
file_credentials = authentication.Credentials.load_from_file()

# Load credentials from environment variables
env_credentials = authentication.Credentials.load_from_env()

# Create client with retrieved & validated credentials
client = authentication.Client(credentials=file_credentials)
```

Contribute
==========
1. Fork the repository
2. Create a feature branch
3. Make changes (including necessary unit tests)
4. Make a pull request and tag the maintainer.

*When making a PR, please include the following information...*
- Summary of issues fixed or changes made
- Explanation of *why* the changes were necessary

Author
=========
Ryan Ferguson

License
=======
GNU GPLv3 - [Project License](https://gitlab.com/RaijinnXIII/lds-python3/blob/master/LICENSE)